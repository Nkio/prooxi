
__author__ = 'Alexandre Cloquet'

from Database import DatabaseConnexion as DBProXi


class KeyWord():

    def __init__(self, tablename, listKeyword):
        self.tablename = tablename
        self.listKeyword = listKeyword
        self.listOfKeyWordDB = DBProXi.DBProoxi().getKeyWordFromCategorie(self.tablename[0])
        self.lenOfListKeywordDB = len(self.listOfKeyWordDB)

    def percentagesOfCompatibility(self):
        percentage = 0.0
        if len(self.listKeyword):
            self.listKeyword = self.cutListKeywordOnList(self.cutListKeyword(self.listKeyword, ','), ' ')
            self.listOfKeyWordDB = list(self.listOfKeyWordDB)
            alreadyKeyword = []
            for keyWordDB in self.listOfKeyWordDB:
                iteKeyword = iter(self.listKeyword)
                for keyword in iteKeyword:
                    if keyWordDB[0] == keyword.encode('utf-8'):
                        if keyWordDB[0] not in alreadyKeyword:
                            alreadyKeyword.append(keyword.encode('utf-8'))
                            # print "KeyWOrd DB = " + keyWordDB[0]
                            # print "KeyWord Site = " + keyword.encode('utf-8')
                            percentage += 1.0
            return percentage * 100 / self.lenOfListKeywordDB
        else:
            return 0

    def cutListKeyword(self, listKeyWord, char=','):
        return listKeyWord[0].split(char)

    def cutListKeywordOnList(self, listKeyWord, char=','):
        listtmp = []
        for keyWord in listKeyWord:
            listtmp += keyWord.split(char)
        return listtmp

    # def percentagesOfCompatibility(self):
    #     percentage = 0.0
    #     self.listKeyword = self.cutListKeywordOnList(self.cutListKeyword(self.listKeyword, ','), ' ')
    #     self.listOfKeyWordDB = list(self.listOfKeyWordDB)
    #     for keyword in self.listKeyword:
    #         for keyWordDB in self.listOfKeyWordDB:
    #             if keyWordDB[0] == keyword.encode('utf-8'):
    #                 print "KeyWOrd DB = " + keyWordDB[0]
    #                 print "KeyWord Site = " + keyword.encode('utf-8')
    #                 keyWordDB.next()
    #                 percentage += 1.0
    #     # print "Pourcentage de mot clef dans la BDD = " + str(percentage)
    #     if len(self.listKeyword) != 0:
    #         coeffOnKeywordDB = float(percentage / self.lenOfListKeywordDB)
    #         coeffOnKeyword = float(percentage / len(self.listKeyword))
    #         # print "Coefficient par rapport la DB = " + str(coeffOnKeywordDB * 100)
    #         # print "Coefficient sur la liste de mot clef = " + str(coeffOnKeyword * 100)
    #         # if coeffOnKeywordDB != 0:
    #         #     print (coeffOnKeyword * 100) / (coeffOnKeywordDB * 100)
    #     # print "Longueur = " + str(len(self.listKeyword))
    #     return percentage
