# coding=utf-8
# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/topics/item-pipeline.html

__author__ = 'Alexandre Cloquet'

from ProoXi.model import Page, Boutique, BlackListUrl, Adresse, Clic, Statistique
from scrapy.exceptions import IgnoreRequest
import tldextract
import datetime
import re


from nltk import wordpunct_tokenize
from nltk.corpus import stopwords

phonePattern = re.compile(r'''(\d{2})\D*(\d{2})\D*(\d{2})\D*(\d{2})\D*(\d*)$''', re.VERBOSE | re.MULTILINE)
address_pattern_fr = re.compile(r'([0-9a-zA-Z,\. ]*) ?([0-9]{5}) ?([a-zA-Z]*)')

regex = re.compile(r'\s+')

indicatif_phone = ["01", "02", "03", "04", "05", "06", "07", "08", "09"] #incatif de telephone
list_balise = ['p', 'h1', 'h2', 'h3', 'h4', 'td', 'span', 'li', 'div', 'strong', 'description'] #Balise à scanner pour trouver les téléphone et adresse


def _calculate_languages_ratios(text):

    languages_ratios = {}

    tokens = wordpunct_tokenize(text)
    words = [word.lower() for word in tokens]

    for language in stopwords.fileids():
        stopwords_set = set(stopwords.words(language))
        words_set = set(words)
        common_elements = words_set.intersection(stopwords_set)

        languages_ratios[language] = len(common_elements)

    return languages_ratios


def detect_language(text):

    ratios = _calculate_languages_ratios(text)

    most_rated_language = max(ratios, key=ratios.get)

    return most_rated_language


from scrapy import log
class ProoxiInsertInModel(object):
    def __init__(self):
        log.msg("__init")
        self.phone_pattern_fr = re.compile(r'''(\d{2})\D*(\d{2})\D*(\d{2})\D*(\d{2})\D*(\d*)$''', re.VERBOSE)
        self.address_pattern_fr = re.compile(r"[0-9]{1,5}(?:(?:[,. ]){1}[-a-zA-Zàâäéèêëïîôöùûüç']+)+", re.VERBOSE)

    def process_item(self, ProoXi, spider):
        for balise in list_balise:
            listtmp = list()
            for elements in ProoXi[balise]:
                elements = elements.strip()
                if len(elements) > 1:
                    listtmp.append(elements)
                ProoXi[balise] = listtmp
            del listtmp

        self.mail = None

        for balise in list_balise:
            self.find_phone(balise, ProoXi)
            if self.mail == None:
                self.mail = self.find_mail(balise, ProoXi)

        if detect_language(ProoXi['body']) != 'french' and tldextract.extract(ProoXi['url']).suffix != 'fr':
            self.black_list_url(ProoXi, detect_language(ProoXi['body']))
            return IgnoreRequest()

        self.make_boutique(ProoXi)
        del self.mail

    def black_list_url(self, ProoXi, language):
        url = self.get_domaine(ProoXi)
        blacklist = BlackListUrl.objects(url=url)
        if not blacklist:
            blacklist = BlackListUrl(url=url,
                                     raison=language,
                                     sousDomaine=tldextract.extract(url).subdomain,
                                     suffix=tldextract.extract(url).suffix,
                                     domaine=tldextract.extract(url).domain)
            blacklist.save()
            del blacklist

    def make_boutique(self, ProoXi):
        domaine = self.get_domaine(ProoXi)

        boutiques = Boutique.objects(urlBase=domaine)
        status = ProoXi['status']
        if len(boutiques):
            Boutique.objects(urlBase=domaine).update_one(set__datePassage=datetime.datetime.utcnow(), set__statut=status)
            self.make_page(ProoXi)
        else:
            adresse = Adresse(telephone=ProoXi['phone'])
            adresse.save()

            title = ProoXi['name']
            boutique = Boutique(datePassage=datetime.datetime.utcnow(),
                                statut=status,
                                nomAffiche=unicode(title, "utf8"),
                                nomFormate=self.strtr(title).lower(),
                                mail=self.mail,
                                urlBase=domaine,
                                active=True,
                                adresses=[adresse, ])
            boutique.save()
            self.make_page(ProoXi)
            del boutique

    def get_domaine(self, ProoXi):
        domaine = str()
        domaine = tldextract.extract(ProoXi['url']).subdomain + "." +\
                  tldextract.extract(ProoXi['url']).domain + "." +\
                  tldextract.extract(ProoXi['url']).suffix
        if domaine.startswith('.'):
            domaine = tldextract.extract(ProoXi['url']).domain + "." + tldextract.extract(ProoXi['url']).suffix
        if domaine != None:
            return domaine

    def make_page(self, ProoXi):
        url = ProoXi['url']
        pagetmp = Page.objects(url=url)
        if not ProoXi['status']:
            ProoXi['status'] = 1
        if len(pagetmp):
            Page.objects(url=url).update_one(set__active=True, set__url=url, set__Title=ProoXi['name'],
                                             set__statut=ProoXi['status'], set__description=str(ProoXi['description']),
                                             set__metaKeyword=ProoXi['keyword'],
                                             set__h1=ProoXi['h1'], set__h2=ProoXi['h2'], set__h3=ProoXi['h3'],
                                             set__p=ProoXi['p'], set__li=ProoXi['li'], set__td=ProoXi['td'])
        else:
            # Si la page n'existe pas
            domaine = self.get_domaine(ProoXi)
            clic = Clic()
            stats = Statistique(nb_clics=[clic,])
            stats.save()
            boutique = Boutique.objects.get(urlBase=domaine)
            page = Page(active=True, url=url,
                        Title=ProoXi['name'],
                        statut=ProoXi['status'],
                        description=str(ProoXi['description']),
                        metaKeyword=ProoXi['keyword'],
                        h1=ProoXi['h1'],
                        h2=ProoXi['h2'],
                        h3=ProoXi['h3'],
                        p=ProoXi['p'],
                        li=ProoXi['li'],
                        td=ProoXi['td'],
                        id_boutique=boutique.id,
                        id_statistique=stats.id)
            page.save()
            del page
        return

    def clean_str(self, str):
        str = str.replace("[u", "")
        str = str.replace("]", "")
        str = str.replace("[", "")
        str = str.replace("\n", "")
        return str

    def strtr(self, chaine):
        try:
            if chaine:
                chaine = self.clean_str(chaine)
                accent = ['é', 'è', 'ê', 'à', 'ù', 'û', 'ç', 'ô', 'î', 'ï', 'â', ' ', ',', '-', '.', '+', '\'', ':', '|', '·']
                sans_accent = ['e', 'e', 'e', 'a', 'u', 'u', 'c', 'o', 'i', 'i', 'a', ' ', '', '', '', '', '', '', '', '']
                chaine = chaine.replace(" ", "")
                for c, s in zip(accent, sans_accent):
                    chaine = chaine.replace(c, s)
                chaine = unicode(chaine, "utf8")
                return chaine
            else:
                return chaine
        except TypeError:
            return chaine
    #
    # def getLemme(self, string):
    #     if string == '':
    #         return ' '
    #
    #     string = self.strtr(string)
    #     return string

    def find_mail(self, balise, ProoXi):
        regex_mail = re.compile(r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*"
                           r"[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
        for p in ProoXi[balise]:
            try:
                result_pattern_mail = regex_mail.findall(regex.sub(' ', p))
                if result_pattern_mail and "@" in str(result_pattern_mail):
                    return result_pattern_mail[0]
            except AttributeError:
                pass

    def find_phone(self, balise, ProoXi):
        """
        Récupère chaque balise et recherche si un numéros de téléphone est dedans.
        regex utilisé = r'''(\d{2})\D*(\d{2})\D*(\d{2})\D*(\d{2})\D*(\d*)$'''
        @param balise: balise à parser voir items.py
        @param ProoXi: Items de scrapy
        @return: Le numéro de téléphone trouvé.
        """
        telephone_tmp = None
        for p in ProoXi[balise]: #scan des éléments des balises
            try:
                result_pattern_phone = phonePattern.search(regex.sub(' ', p)).groups()
                telephone = str()
                for ele in result_pattern_phone:
                    telephone += ele
                for indicatif in indicatif_phone:
                    if telephone.startswith(indicatif) and telephone.isdigit() and len(telephone) == 10:
                        telephone_tmp = telephone
            except AttributeError:
                pass
        ProoXi['phone'] = telephone_tmp
        del telephone_tmp
