# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html

from scrapy.item import Item, Field

__author__ = 'Alexandre Cloquet'


class ProoxiItem(Item):
    url = Field()
    name = Field()
    description = Field()
    keyword = Field()
    link = Field()
    status = Field()
    h1 = Field()
    h2 = Field()
    h3 = Field()
    h4 = Field()
    p = Field()
    body = Field()
    td = Field()
    div = Field()
    span = Field()
    li = Field()
    strong = Field()
    adresse = Field()
    phone = Field()
