# coding=utf-8
# Scrapy settings for ProoXi project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'ProoXi'

SPIDER_MODULES = ['ProoXi.spiders']
NEWSPIDER_MODULE = 'ProoXi.spiders'

DOWNLOADER_MIDDLEWARES = {
    'scrapy.contrib.downloadermiddleware.cookies.CookiesMiddleware': 700,
    'ProoXi.Middleware.ExcludeDomainMiddleware' : 675,
    'ProoXi.Middleware.RandomUserAgentMiddleware': 400,
    'scrapy.contrib.downloadermiddleware.useragent.UserAgentMiddleware': 100,
}

#SPIDER_MIDDLEWARES = {'ProoXi.middlewares.IgnoreVisitedItems': 560}

ITEM_PIPELINES = {
    #'ProoXi.pipelines.ProoxiCheckKeyWordPipeline': 50,
    'ProoXi.pipelines.ProoxiFindPhoneNumber': None,
    'ProoXi.pipelines.ProoxiPipeline': None,
    'ProoXi.pipelines.ProoxiInsertInModel': 100,
    }

EXTENSIONS = {'ProoXi.MailSender.StatusMailer': None,
              'scrapy.contrib.feedexport.FeedExporter': None,
              'scrapy.webservice.WebService': 500,
              'scrapy.telnet.TelnetConsole': 500}

DATABASE = {'drivername': 'mysql',
            'host': 'localhost',
            'port': '3306',
            'username': 'root',
            'password': 'root',
            'database': 'prooxi8-2'}

DEFAULT_REQUEST_HEADERS = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Encoding': ['x-gzip,gzip,deflate'],
    'Accept-Language': 'fr',
}

AJAXCRAWL_ENABLED = True

LOG_ENABLED = True
LOGSTATS_INTERVAL = 30.0
RETRY_ENABLED = False
RETRY_TIMES = 2
DOWNLOAD_TIMEOUT = 10

CONCURRENT_REQUESTS = 110
COOKIES_ENABLED = False
CONCURRENT_ITEMS=100
#production
LOG_LEVEL = 'INFO'

REDIRECT_ENABLED = False
DNSCACHE_ENABLED = True
ROBOTSTXT_OBEY = True
DOWNLOAD_DELAY = 2
DEPTH_LIMIT = 5

#CONCURRENT_REQUESTS_PER_IP = 1
CONCURRENT_REQUESTS_PER_DOMAIN = 10
AUTOTHROTTLE_ENABLED = True
#RETRY_HTTP_CODES = [500, 503, 504, 400, 404, 408]
#WEBSERVICE_ENABLED = True

COEFF_KEYWORD = 1.5
COEFF_DESC = 4
COEFF_NAME = 3
COEFF_H2 = 0.8
# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'ProoXiBot'

USER_AGENT_LIST = ['ProoXiBot',
                   'BotProoXi',
                   'GoogleBot',
                   'TestBot']

CATEGORY_LIST = ["agence immobilière",
                 "agence de voyage"]

STATUSMAILER_RECIPIENTS = ['jcchauvin@prooxi.fr']
#STATUSMAILER_COMPRESSION = 'gzip'
STATUSMAILER_COMPRESSION = None

DEPTH_PRIORITY = 1
SCHEDULER_DISK_QUEUE = 'scrapy.squeue.PickleFifoDiskQueue'
SCHEDULER_MEMORY_QUEUE = 'scrapy.squeue.FifoMemoryQueue'

MAIL_HOST = 'smtp.gmail.com'
MAIL_PORT = 587
MAIL_USER = 'alexandre.cloquet@gmail.com'
MAIL_PASS = 'rkcoYnd7'

#JOBDIR = '/home/prooxi/robot/prooxi/job/'
