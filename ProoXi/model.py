__author__ = 'Alexandre Cloquet'

from mongoengine import *

connect('px_boutique_test27', host="91.121.3.6", w=0)


class Ville(Document):
    nom = StringField()


class Clic(EmbeddedDocument):
    date = DateTimeField()
    ip = StringField(max_length=15)
    locs = PointField()


class Statistique(Document):
    nb_clics = ListField(EmbeddedDocumentField(Clic))
    date_dernier_clic = DateTimeField()
    moyenne_clic_heure = IntField()
    moyenne_clic_jour = IntField()
    moyenne_clic_semaine = IntField()
    moyenne_clic_mois = IntField()


class Boutique(Document):
    datePassage = DateTimeField()
    statut = IntField()
    nomAffiche = StringField()
    nomFormate = StringField()
    mail = EmailField()
    urlBase = StringField()
    active = BooleanField()
    adresses = ListField(ReferenceField("Adresse"))


class Page(Document):
    active = BooleanField()
    url = StringField()
    Title = StringField()
    statut = IntField()
    description = StringField()
    metaKeyword = ListField(StringField())
    h1 = ListField(StringField())
    h2 = ListField(StringField())
    h3 = ListField(StringField())
    p = ListField(StringField())
    li = ListField(StringField())
    td = ListField(StringField())
    id_boutique = ReferenceField(Boutique)
    id_statistique = ReferenceField(Statistique)


class Adresse(Document):
    numeroRue = IntField()
    typeVoie = StringField(max_length=50)
    nomRue = StringField
    complementAdresse = StringField()
    telephone = StringField()
    mail = EmailField()
    ville = ReferenceField(Ville)
    horaire_lundi_matin = StringField()
    horaire_lundi_soir = StringField()
    horaire_mardi_matin = StringField()
    horaire_mardi_soir = StringField()
    horaire_mercredi_matin = StringField()
    horaire_mercredi_soir = StringField()
    horaire_jeudi_matin = StringField()
    horaire_jeudi_soir = StringField()
    horaire_vendredi_matin = StringField()
    horaire_vendredi_soir = StringField()
    horaire_samedi_matin = StringField()
    horaire_samedi_soir = StringField()
    horaire_dimanche_matin = StringField()
    horaire_dimanche_soir = StringField()
    id_boutique = ReferenceField(Boutique)


class BlackListUrl(Document):
    url = StringField()
    raison = StringField()



