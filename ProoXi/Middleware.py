__author__ = 'Alexandre Cloquet'

from settings import USER_AGENT_LIST
import random
from scrapy import log
from scrapy.exceptions import IgnoreRequest
from ProoXi.model import BlackListUrl
import tldextract


blackListUrl = []
for BlackListUrl in BlackListUrl.objects():
    blackListUrl.append(BlackListUrl.url)


class RandomUserAgentMiddleware(object):

    def process_request(self, request, spider):
        ua = random.choice(USER_AGENT_LIST)
        if ua:
            request.headers.setdefault('User-Agent', ua)
            #log.msg('>>>> UA %s' % request.headers)


class ExcludeDomainMiddleware(object):

    def process_response(self, request, response, spider):
        log.msg("In Middleware " + response.url, level=log.INFO)
        tld = tldextract.extract(response.url)
        if tld.domain in blackListUrl:
            log.msg("Exclude " + response.url, level=log.WARNING)
            IgnoreRequest()
        return response