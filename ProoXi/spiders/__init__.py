# This package will contain the spiders of your Scrapy project
#
# Please refer to the documentation for information on how to create and manage
# your spiders.

__author__ = 'Alexandre Cloquet'

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from Database import DatabaseConnexion as DBProXi
from CheckUrl import CheckUrl as check
from ProoXi.items import ProoxiItem
from ProoXi.model import Boutique
import random


class ProoXiSpider(CrawlSpider):
    name = 'ProoXi'
    allowed_domains = []
    #i = [test for test in Boutique.objects.only("urlBase")]
    #i = len(i)
    urls = ["http://" + test.urlBase for test in Boutique.objects.only("urlBase").limit(10).order_by('id')]
    start_urls = [l.strip() for l in urls]
    #checkurl = check.CheckUrl()
    del urls
    rules = (Rule(SgmlLinkExtractor(allow=()), callback='parse_', follow=True), )

    def parse_(self, response):
        x = Selector(response)
        ProoXi = ProoxiItem()
        ProoXi['url'] = response.url
        try:
            ProoXi['name'] = x.xpath("//title/text()").extract()[0].encode('utf-8')
        except IndexError:
            ProoXi['name'] = str(x.xpath("//title/text()").extract()).encode('utf-8')
        if not ProoXi['name']:
            ProoXi['name'] = x.xpath('//meta[@property="og:title"]/@content').extract()[0].encode('utf-8')

        ProoXi['description'] = x.xpath('//meta[@name="description"]/@content').extract()
        if not ProoXi['description']:
            ProoXi['description'] = x.xpath('//meta[@property="description"]/@content').extract()
        if not ProoXi['description']:
            ProoXi['description'] = x.xpath('//meta[@name="Description"]/@content').extract()
        if not ProoXi['description']:
            ProoXi['description'] = x.xpath('//meta[@name="DESCRIPTION"]/@content').extract()
        if not ProoXi['description']:
            ProoXi['description'] = x.xpath('//META[@NAME="description"]/@content').extract()
        if not ProoXi['description']:
            ProoXi['description'] = x.xpath('//META[@NAME="Description"]/@content').extract()
        if not ProoXi['description']:
            ProoXi['description'] = x.xpath('//META[@NAME="DESCRIPTION"]/@content').extract()
        print "Prooxi Description %s" % ProoXi['description']
        ProoXi['keyword'] = x.xpath('//meta[@name="keywords"]/@content').extract()
        if not ProoXi['keyword']:
            ProoXi['keyword'] = x.xpath('//meta[@property="keywords"]/@content').extract()
        if not ProoXi['keyword']:
            ProoXi['keyword'] = x.xpath('//meta[@name="Keywords"]/@content').extract()
        if not ProoXi['keyword']:
            ProoXi['keyword'] = x.xpath('//meta[@name="KEYWORDS"]/@content').extract()
        if not ProoXi['keyword']:
            ProoXi['keyword'] = x.xpath('//META[@NAME="keywords"]/@content').extract()
        if not ProoXi['keyword']:
            ProoXi['keyword'] = x.xpath('//META[@NAME="Keywords"]/@content').extract()
        if not ProoXi['keyword']:
            ProoXi['keyword'] = x.xpath('//META[@NAME="KEYWORDS"]/@content').extract()

        ProoXi['status'] = response.status
        ProoXi['h1'] = x.xpath('//h1/text()').extract()
        ProoXi['h2'] = x.xpath('//h2/text()').extract()
        ProoXi['h3'] = x.xpath('//h3/text()').extract()
        ProoXi['p'] = x.xpath('///p/text()').extract()
        ProoXi['h4'] = x.xpath('//h4').extract()
        ProoXi['span'] = x.xpath('//span/text()').extract()
        ProoXi['li'] = x.xpath('//li/text()').extract()
        ProoXi['strong'] = x.xpath('//strong/text()').extract()
        ProoXi['body'] = response.body
        x.remove_namespaces()
        ProoXi['adresse'] = x.xpath('/address').extract()
        ProoXi['td'] = x.xpath('//td/text()').extract()
        ProoXi['div'] = x.xpath('//div/text()').extract()
        del response
        yield ProoXi

SPIDER = ProoXiSpider()
