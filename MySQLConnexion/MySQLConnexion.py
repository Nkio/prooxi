# -*- coding: utf-8 -*-

__author__ = 'Alexandre Cloquet'

import MySQLdb
from scrapy import log
import sys


class MySQLConnexion:
    connexion = None

    def __init__(self,
                 adress="176.31.253.73",
                 username='DevProoXi',
                 password='rkcoYnd7',
                 dbname='prooxi8-0'):
        try:
            self.connexion = MySQLdb.connect(adress, username, password, dbname)
        except MySQLdb.Error, e:
            log.msg("Error %d: %s" % (e.args[0], e.args[1]), level=log.ERROR)
            self.connexion.close()
            sys.exit(1)

    def __del__(self):
        if self.connexion:
            log.msg("Closing connection MySQL", level=log.DEBUG)
            self.connexion.close()

    def select(self, request, param=None):
        cur = self.connexion.cursor()
        if param:
            try:
                cur.execute(request, param)
            except MySQLdb.MySQLError, e:
                log.msg("Error %d: %s" % (e.args[0], e.args[1]), level=log.ERROR)
        else:
            try:
                cur.execute(request)
            except MySQLdb.MySQLError, e:
                log.msg("Error %d: %s" % (e.args[0], e.args[1]), level=log.ERROR)
        data = cur.fetchall()
        return data

    def update(self, request, param=None):
        cur = self.connexion.cursor()
        if param:
            try:
                cur.execute(request, param)
            except MySQLdb.MySQLError, e:
                log.msg("Error %d: %s" % (e.args[0], e.args[1]), level=log.ERROR)
        else:
            try:
                cur.execute(request)
            except MySQLdb.MySQLError, e:
                log.msg("Error %d: %s" % (e.args[0], e.args[1]), level=log.ERROR)

    def insert(self, request, param=None):
        cur = self.connexion.cursor()
        if param:
            try:
                cur.execute(request, param)
            except MySQLdb.MySQLError, e:
                log.msg("Error %d: %s" % (e.args[0], e.args[1]), level=log.ERROR)
                log.msg("Rollback because error %d: %s" % (e.args[0], e.args[1]), level=log.DEBUG)
                self.connexion.rollback()
        else:
            try:
                cur.execute(request)
            except MySQLdb.MySQLError, e:
                log.msg("Error %d: %s" % (e.args[0], e.args[1]), level=log.ERROR)
                log.msg("Rollback because error %d: %s" % (e.args[0], e.args[1]), level=log.DEBUG)
                self.connexion.rollback()
